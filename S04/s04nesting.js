class Employee {
  constructor(name, email, sales) {
    this.name = name;
    this.email = email;
    if (sales.length === 4) {
      if (
        sales.every((indiv_sales) => indiv_sales >= 0 && indiv_sales <= 100)
      ) {
        this.sales = sales;
      } else {
        this.sales = undefined;
      }
    } else {
      this.sales = undefined;
    }
    this.salesAve = undefined;
    this.metQuota = undefined;
    this.earnedBonus = undefined;
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }

  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }

  listSales() {
    console.log(
      `${this.name} has quarterly sales of ${this.computeAve(this.sales)}`
    );
    return this;
  }

  computeAve() {
    let sum = 0;
    this.sales.forEach((indiv_sale) => {
      sum += indiv_sale;
    });
    this.salesAve = sum / 4;
    return this;
  }

  hasMetQuota() {
    this.metQuota = this.computeAve().salesAve >= 85 ? true : false;
    //we need to call salesAve we change computeAve's return from sum/4 to "this"
    //when we did that, the output of computeAve() because the object itself, so we need to
    //access that property salesAve
    return this;
  }

  willGetBonus() {
    this.earnedBonus =
      this.hasMetQuota().metQuota && this.computeAve().salesAve >= 90
        ? true
        : false;
    return this;
  }
}

/////////////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
class TeamLead {
  constructor(name, email, country, team, tenure) {
    this.name = name;
    this.email = email;
    this.country = country;
    this.team = team;
    this.tenure = tenure;
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }

  getCountryAndTeam() {
    console.log(`${this.name} works in ${this.country}'s ${this.team}`);
    return this;
  }

  increaseTenure(years) {
    //let add = window.prompt("Enter years to be added: ");
    this.tenure += parseInt(years);
    return this;
  }
  getTenure() {
    console.log(
      `${this.name} has worked for ${this.tenure} years in the company`
    );
    return this;
  }
}

/////////////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

//Create a new class called Team
class Team {
  //every Team object created will only have a name, but will have an empty array for its members
  constructor(name) {
    this.name = name;
    this.teamLead = undefined;
    this.members = [];
    this.bonusEarners = undefined;
    this.bonusEarnersPercentage = undefined;
  }

  //we have to create a method that would add members to the team
  //the addNumber method would take names, emails, and sales
  //In this part, we weill instatiate/ create an employee inside the medthod,
  //and push the new employee to the members array.
  addTeamLead(name, email, country, tenure) {
    this.teamLead = new TeamLead(name, email, country, this.name, tenure);
    return this;
  }

  addMember(name, email, sales) {
    this.members.push(new Employee(name, email, sales));
    //with this, a team object can have Employees object as members
    return this;
  }

  countBonusEarners() {
    let count = 0;
    this.members.forEach((member) => {
      if (member.willGetBonus().earnedBonus) {
        count++;
      }
    });
    this.bonusEarners = count;
    return this;
  }
  getBonusEarnersPercentage() {
    this.bonusEarnersPercentage =
      (this.countBonusEarners().bonusEarners / this.members.length) * 100;
    return this;
  }
}

//Q: how to create a new team
//Ex create a team with name Manila Sales Team

let team1 = new Team("Manila Sales Team");
team1.addMember("John", "john@mail.com", [89, 84, 78, 88]);
team1.addMember("Jane", "jane@mail.com", [98, 87, 88, 78]);
team1.addMember("Joe", "joe@mail.com", [45, 80, 87, 89]);
team1.addMember("Jessica", "jessica@mail.com", [98, 99, 85, 100]);

//when addMember is call, ti gets the arguments and creates a new
console.log(team1);
//Q. Is it possible to access the members of team1?
//Ex. what if we want to list Jessica's sales
team1.members[3].listSales;

//Mini exercise
//determine if joe has met quota
console.log(team1.members[2].hasMetQuota().metQuota);
//determine if john will get bonus
console.log(team1.members[0].willGetBonus().earnedBonus);
console.log(team1.countBonusEarners().bonusEarners);
console.log(team1.getBonusEarnersPercentage().bonusEarnersPercentage);

//exercise 1 )
//1 copy the TeamLead from s03classes.js (hint, place the TeamLead class before the Team Class)
//2 modify the team class so that there is a property for the team lead
//3 modify the Team class so that there is method to add a team lead based on the parameters needed by the Team Lead Class
//4 add a team to team1
team1.addMember("John", "john@mail.com", [89, 84, 78, 88]);

team1.addTeamLead("Bruce", "bruce@mail.com", "USA", 3);

console.log(team1);
console.log(team1.teamLead);

//Define a Company class whose constructor will accept a string argumanet to serve as its name
// - name initialized to passed to string argument
// profict Centercrs

class Company {
  constructor(name) {
    this.name = name;
    this.profitCenters = [];
    this.totalEmployees = 0;
    this.totalBonusEarners = 0;
    this.annualAveSales = undefined;
    this.annualMinSales = undefined;
    this.annualMaxSales = undefined;
  }

  addProfitcenters(name) {
    this.profitCenters.push(new Team(name));
    return this;
  }

  countEmployees() {
    this.profitCenters.forEach(
      (team) => (this.totalEmployees += team.members.length)
    );
    return this;
  }
}

//Create a company called Acme Corp
//Create a team named Marketing in Acme Corp

let company1 = new Company("Acme Corp");
company1.addProfitcenters("Marketing");

//add the teams "Sales", "Research", "Production"
company1.addProfitcenters("Sales");
company1.addProfitcenters("Research");
company1.addProfitcenters("Production");

company1.profitCenters
  .find((team) => team.name === "Sales")
  .addMember("John", "john@mail.com", [89, 84, 78, 88]);
//.find() is used to filter the first element in the array that matches the criteria in the function

company1.profitCenters
  .find((t) => t.name === "Marketing")
  .addMember("Joe", "joe@mail.com", [85, 98, 75, 70]);

company1.profitCenters
  .find((t) => t.name === "Research")
  .addMember("Jessica", "jessica@mail.com", [99, 99, 87, 100]);

console.log(company1.profitCenters);

console.log(company1.countEmployees().totalEmployees);

//team1.addMember("John", "john@mail.com", [89, 84, 78, 88]);
