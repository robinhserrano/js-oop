//In JS classe can be created using the "class" keyword and ();
//Naming convention for classes: Begin with UpperCase characters
/*
Syntax
class <Name> {

}

*/

class Employee {
  //The "new" keyword will look for a constructor method in the class would allow it to get
  //values upon instantiation
  //The constructor methoeds tell the program HOW an object will be created.
  //To have employees have names and emails upon creation, our constructor must have a name and email argumaent in it.
  constructor(name, email, sales) {
    this.name = name;
    this.email = email;
    if (sales.length === 4) {
      //
      if (
        sales.every((indiv_sales) => indiv_sales >= 0 && indiv_sales <= 100)
      ) {
        this.sales = sales;
      } else {
        this.sales = undefined;
      }
    } else {
      this.sales = undefined;
    }

    this.salesAve = undefined;
    this.metQuota = undefined;
    this.earnedBonus = undefined;
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }

  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }

  listSales() {
    console.log(
      `${this.name} has quarterly sales of ${this.computeAve(this.sales)}`
    );
    return this;
    //returns the object itself
  }

  computeAve() {
    let sum = 0;
    this.sales.forEach((indiv_sale) => {
      sum += indiv_sale;
    });
    //return total / 4;
    this.salesAve = sum / 4;
    return this;
  }

  hasMetQuota() {
    this.metQuota = this.computeAve().salesAve >= 85 ? true : false;
    return this;
  }

  willGetBonus() {
    this.earnedBonus =
      this.hasMetQuota().metQuota && this.computeAve().salesAve >= 90
        ? true
        : false;
    return this;
  }
}

//What happens? whenever we create an object, we need to supply names and emails and it
//automaticall assin them to the new object (hence the keyword this.)

//INSTANTIATION - process of creating objects from classes.
//To create an object from a class, use the "new" keyword

//Medthods in custom objects are Comma seperated while method in classes ARE NOT

let employeeOne = new Employee("John", "john@mail.com", [89, 84, 78, 88]);
let employeeTwo = new Employee("Jane", "jane@mail.com", [78, 82, 79, 85]);
let employeeThree = new Employee(
  "Brandon",
  "brandon@mail.com",
  [15, 0, 10, 100]
);

//Each object created using the new keyword an INSTANCE of the class
// Q: what would be the expected output when the following line is done
console.log(employeeOne);
console.log(employeeTwo);
console.log(employeeThree);
employeeThree.listSales();
employeeTwo.email = "janeDoe@email.com";
console.log(employeeTwo.email);
// is this best practice?, No this poses a possible security risk.
//This is not best practice, as the best practice is to regulate such access to the props.
//This can be done using getters (regulates retrieval of values) and setters (regulates manipulation)

//Method chaining
//In order for methods to be chained, simply do the dot notations for the methods
console.log("-----------------------------------------");
employeeTwo.listSales().logout();
//Q: what would happen? the chaining failed because ListSales didn't have ANY return value
//Q: What would where does logout come from?
//In order to remedy the undefined value, each method should RETURN some value, in this case, we return "this"
//after doing the return, the employeeOne.listSales().logout() is transformed into employeeOne.listSales() -> employeeOne.logout()
employeeOne.hasMetQuota().willGetBonus();
console.log("A", employeeOne.earnedBonus);

//Mini activity 2:
/*
create new employee
name: Jessica
email: jessica@mail.com
sales: [98,98,87,100]
2. use the method to get her sales
3. use method chaining to know if she would get bonus and logout
*/
let employeeFour = new Employee(
  "Jessica",
  "jessica@mail.com",
  [98, 98, 87, 100]
);
employeeFour.listSales();
employeeFour.willGetBonus().logout();
console.log("B", employeeFour.earnedBonus);

///////////////////////////////////////////
//create a class for the team lead based on the object definition from s02Objects.js
//create 2 team leads (choose any) value

class TeamLead {
  constructor(name, email, country, team, tenure) {
    this.name = name;
    this.email = email;
    this.country = country;
    this.team = team;
    this.tenure = tenure;
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }

  getCountryAndTeam() {
    console.log(`${this.name} works in ${this.country}'s ${this.team}`);
    return this;
  }

  increaseTenure(years) {
    //let add = window.prompt("Enter years to be added: ");
    this.tenure += parseInt(years);
    return this;
  }
  getTenure() {
    console.log(
      `${this.name} has worked for ${this.tenure} years in the company`
    );
    return this;
  }
}

let teamLeadOne = new TeamLead(
  "Bruce",
  "bruce@mail.com",
  "USA",
  "Development Team",
  3
);

let teamLeadTwo = new TeamLead(
  "Wayne",
  "wayne@mail.com",
  "USA",
  "Sales Support",
  4
);
