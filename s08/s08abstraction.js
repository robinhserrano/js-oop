//Make Person into an abstract class, as an abstract class, Person can not be instantiated
//To do this, we need to change the constructor of the Person class and disable creation

//Making an abstract class is a way to safeguard classes in programming

class Person{
    //We change the constructor into an empty constructor
    //Then we add a check to see if we instantiated from Person and throw an error
    constructor(){
        //checks if we used the new Person()
    if(this.constructor === Person){
        throw new Error("Object cannot be created from an abstract class Person");
    }
        
    //Make the method getFullName() required
    if(this.getFullName === undefined){
        throw new Error("Class must implement getFullName() method");
    }

    if(this.getFirstName === undefined){
        throw new Error("Class must implement getFirstName() method");
    }

    if(this.getLastName === undefined){
        throw new Error("Class must implement getLastName() method");
    }
  }
}
  
    
class Employee extends Person{
    constructor(firstName,lastName, employeeID){
    super()
    this.firstName = firstName
    this.lastName = lastName
    this.employeeID = employeeID
    }
    //Mini exercise 1: Create a getFullName()
    getFullName(){
      return `\n${this.firstName} ${this.lastName} has employeeID of ${this.employeeID}`
    }

    getFirstName(){
        return `\nFirst Name: ${this.firstName}`
      }

    getLastName(){
        return `\nLast Name: ${this.lastName}`
      }
  }

const employee1 = new Employee("John", "Doe", "Acme-001")
console.log(employee1.getFullName())


class Student extends Person{
    constructor(firstName,lastName, studentID, section){
    super()
    this.firstName = firstName
    this.lastName = lastName
    this.studentID = studentID
    this.section = section
    }
    getFullName(){
      return `\nFull Name: ${this.firstName} ${this.lastName}`
    }

    getFirstName(){
        return `\nFirst Name: ${this.firstName}`
      }

    getLastName(){
        return `\nLast Name: ${this.lastName}`
      }

    getStudentDetails(){
        return `\n${this.getFullName()} with a student ID of ${this.studentID}, belongs to the section ${this.section} `
      }
  }


const employee2 = new Employee("Jack","Bill","Acme-002")
console.log(employee1.getFirstName())
console.log(employee1.getLastName())

const student1 = new Student("Brandon","Boyd","GenSc-100","4-Kalakuchi")
console.log(student1.getFullName())
console.log(student1.getStudentDetails())

//Exercise 1:
//1. Convert the RegularShape class from s07polymorphism to an abstract class and add the 
// necessarry error message as needed
//2. Modify the square class as needed 


class RegularShape{
    constructor(){
        if(this.constructor === RegularShape){
            throw new Error("Object cannot be instantiated from an abstract class RegularShape");
        }
            
        if(this.getPerimeter === undefined){
            throw new Error("Method getPerimeter needs to be implemented");
        }
    
        if(this.getArea === undefined){
            throw new Error("Method getArea needs to be implemented");
        }
      }
  }
  
  
  class Triangle extends RegularShape{
    constructor(noSides,length){
        super()
        this.noSides = noSides
        this.length = length
       
        }
      getPerimeter(){
        return`the parameter of the triangle is ` + (this.noSides * this.length) 
      }
  
      getArea(){
        return `the area of the triangle is `+ (this.length * this.length  * Math.sqrt(3) /4)
      }
  }
  
  class Square extends RegularShape{
    constructor(noSides,length){
    super()
    this.noSides = noSides
    this.length = length
   
        }
        getPerimeter(){
            return`the parameter of the square is ` + (this.length * 4) 
        }
        getArea(){
            return `the area of the square is ` + this.length * this.length;
        }
  }

let triangle1 = new Triangle(3,10)
console.log(triangle1.getPerimeter())
console.log(triangle1.getArea())
let square1 = new Square(4,12);
console.log(square1.getPerimeter())
console.log(square1.getArea())


class Food{
    constructor(name){
        this.name = name
        if(this.constructor === Food){
            throw new Error("Object cannot be instantiated from an abstract class Food");
        }
            
        if(this.getName === undefined){
            throw new Error("Method getName needs to be implemented");
        }
    
      }
  }
  

class Vegetable extends Food{
    constructor(name, breed, price){
        super(name)
        this.breed = breed
        this.price = price
    }
    getName(){
        return `${this.name} is of ${this.breed} variety and is priced at ${this.price} PHP`
    }
  }


  const vegetable1 = new Vegetable("Pechay", "Native", 25)
  console.log(vegetable1.getName())

  //Ex2
  //1. Convert the equipment class into an abstract class with properties, equipmentType and
  //model as well needs to implement a method calle printInfo
  //2. Choose 2 of the 3 child class(Tower Crane, Bulldozer, Backloader) and convert them as
  //well as needed, especially for the printInfo method

  class Equipment{
    constructor(equipmentType, model){
        this.equipmentType = equipmentType 
        this.model = model 
        if(this.constructor === Equipment){
            throw new Error("Object cannot be instantiated from an abstract class Equipment");
        }
            
        if(this.printInfo === undefined){
            throw new Error("printInfo() must to be implemented");
        }
   
    }
    printInfo(){
      return `\nInfo: {this.equipmentType} has a model of ${this.model}`
    }
  }
  
  class  Bulldozer extends Equipment{
    constructor(equipmentType,bladeType,model){
      super(equipmentType, model) 
      this.bladeType = bladeType
      this.model = model
      }
  
      printInfo(){
        return `Info: ${this.equipmentType} \n\nThe bulldozer ${this.model} has a ${this.bladeType}`
      }
  }
  
  class TowerCrane extends Equipment{
    constructor(equipmentType,model, hookRadius, maxCapacity){
        super(equipmentType, model) 
     
      this.model = model
      this.hookRadius = hookRadius
      this.maxCapacity = maxCapacity
      }
  
      printInfo(){
        return `Info: ${this.equipmentType} \n\nThe tower crane ${this.model} has a ${this.hookRadius} cm hook radius and  ${this.maxCapacity}  kg max capacity`
      }
  }


let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel")
console.log(bulldozer1.printInfo())
let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towerCrane1.printInfo())
