//Serrano Robin Andrei H. - robinhserrano@gmail.com
class Customer {
  constructor(email) {
    this.email = email;
    this.order = new Order();     //Creates an instance of the Order Class
    this.orders = [];             //array of objects with structure {projects: order contents,totalAmount:order total}
  }

  checkOut() { 
    if(this.order.contents.length !==0){ 
        this.order.generateInvoice()
        this.orders.push({order: this.order.contents, totalAmount:this.order.totalAmount})
        this.order.clearOrderContents()
        console.log("Check Out")
        //console.log("Check Out", this.orders) // for debugging purposes
    }
    return this
  }
}

class Order {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToOrder(projName, qty){ 
      if(projName.isActive === true){
          this.contents.push({project:projName, quantity:qty});
      }else{
          console.log("asset tracker is archived and may no longer be ordered")
      }
      return this
  }

  showOrderContents(){
      console.log("showOrderContents",this.contents)  
      return this
  }

  updateProjectQuantity(projName, newQty){ //
     
      for(let i = 0; i < this.contents.length; i++) {
      if(this.contents[i].project.name === projName){
          this.contents[i].quantity += newQty;
         }
      }

      console.log("updateProjectQuantity", this.contents)
      
      return this
  }
  clearOrderContents(){
      this.contents = []
      this.totalAmount = 0;
      return this
  }
  generateInvoice(){
      this.totalAmount = 0

      for(let i = 0; i < this.contents.length; i++) {
          this.totalAmount += this.contents[i].project.price * this.contents[i].quantity;
  }
  console.log("Generate Invoice", {contents: this.contents, totalAmount: this.totalAmount})
  return this
}
  
}

class Project { 
  constructor(name,price) {
    this.name = name;
    this.price = price;
    this.isActive = true; 
  }

  archive(){ 
      if(this.isActive === true){
          this.isActive = false;
      }
      return this
  }
  updatePrice(newPrice){ 
      this.price = newPrice
      return this
  }
}

//TEST
const acmeCo = new Customer('admin@acmeco.com')

const projA = new Project('asset tracker', 50000)
const projB = new Project('working schedulting app', 60000)
const projC = new Project('issues tracking', 45000)
const projD = new Project('project management suite', 100000)

projC.archive()
acmeCo.order.addToOrder(projA,3)
acmeCo.order.addToOrder(projB,2)
acmeCo.order.addToOrder(projC,2)

acmeCo.order.generateInvoice()                        //Will print in the console - Y

acmeCo.order.updateProjectQuantity('asset tracker',2) //Will print in the console - Y
acmeCo.order.generateInvoice()                        //Will print in the console - Y

acmeCo.order.showOrderContents()                      //Will print in the console - Y
acmeCo.order.clearOrderContents()
acmeCo.order.addToOrder(projD,2)                      //Will print in the console - Y

console.log(acmeCo.checkOut())                        //Will print in the console - Y