1.) []                           Correct
2.) True 	                 Wrong
3.) False 	                 Correct	
4.) replace()		         Correct	
5.) lastIndexOf() 	 	 Correct	

6.) {}				 Correct
7.) TRUE			 Correct
8.) key1: value1, key2: value2   Correct
9.) 0				 Correct
10) TRUE			 Correct

11.) TRUE			 Correct
12.) forEach()			 Correct
13.) True			 Correct
14.) Encapsulation		 Correct
15.) minimizes repetitions, improves scalability, improves maintainability     (Correct)

16.) True			 Correct
17.) ? <if condition is true> : <if condition is false>	 Correct		
18.) map()			 Correct
19.) this			 Correct
20.) TRUE			 Correct

19/20