let employeeOne = {
name: "John",
email: "john@email.com",
sales: [89,84,78,88],

login(){
    console.log(`${this.email} has logged in`)
},
logout(){
    console.log(`${this.email} has logged out`)
},

listSales(){
    console.log(`${this.name} has quarterly sales of ${this.computeAve(this.sales)}`)
},

computeAve(){
    let total = 0
    this.sales.forEach(indiv_sale => {
        total += indiv_sale
    });
return total/4
},

hasMetQuota(){
    return this.computeAve() >=85;
    },

willGetBonus(){
    return this.hasMetQuota() && this.computeAve()>=90;
        }
}

let employeeTwo = {
name: "Joe",
email: "joe@email.com",
sales: [78, 82, 79, 85],

login(){
    console.log(`${this.email} has logged in`)
},
logout(){
    console.log(`${this.email} has logged out`)
},

listSales(){
    console.log(`${this.name} has quarterly sales of ${this.computeAve(this.sales)}`)
},

computeAve(){
    let total = 0
    this.sales.forEach(indiv_sale => {
        total += indiv_sale
    });
return total/4
},

hasMetQuota(){
    return this.computeAve() >=85;
    },

willGetBonus(){
    return this.hasMetQuota() && this.computeAve()>=90;
        }
}

let employeeThree = {
    name: "Jane",
    email: "jane@email.com",
    sales: [87, 89, 91, 93],
    
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.computeAve(this.sales)}`)
    },
    
    computeAve(){
        let total = 0
        this.sales.forEach(indiv_sale => {
            total += indiv_sale
        });
    return total/4
    },
    
    hasMetQuota(){
        return this.computeAve() >=85;
        },
    
    willGetBonus(){
        return this.hasMetQuota() && this.computeAve()>=90;
            }
    }

let employeeFour = {
    name: "Jessica",
    email: "jessica@email.com",
    sales: [91, 89, 92, 93],
    
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.computeAve(this.sales)}`)
    },
    
    computeAve(){
        let total = 0
        this.sales.forEach(indiv_sale => {
            total += indiv_sale
        });
    return total/4
    },
    
    hasMetQuota(){
        return this.computeAve() >=85;
        },
    
    willGetBonus(){
        return this.hasMetQuota() && this.computeAve()>=90;
            }
    }

let teamLead = {
    name: "Bob",
    email: "bob@email.com",
    country: "Germany",
    team: "Sales support",
    tenure: 3,

    getCountryAndTeam(){
        console.log(`${this.name} works in ${this.country}'s ${this.team}`)

    },
    increaseTenure(years){
        //let add = window.prompt("Enter years to be added: ");
        this.tenure += parseInt(years);
    },
    getTenure(){
        console.log(`${this.name} has worked for ${this.tenure} years in the company` )
    },

    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
}

console.log(teamLead.login())
console.log(teamLead.logout())
console.log(teamLead.getCountryAndTeam())
console.log(teamLead.getTenure())

const teamBob = {
    lead: teamLead,
    employees: [employeeOne,employeeTwo,employeeThree,employeeFour],
    countBonusEarners(){
    let count = 0;
    for(i=0;i<this.employees.length;i++){
        if(this.employees[i].willGetBonus()){
            count++
        }
    }
    return count
    },

    getBonusEarnerPercentage(){
        console.log(`${this.countBonusEarners()*(100)/this.employees.length}% of the team will receive bonuses this year`)
    }
}

console.log(teamBob.lead.country);
console.log(teamBob.employees[1].computeAve());
console.log(teamBob.countBonusEarners());
teamBob.getBonusEarnerPercentage();


// console.log(employeeThree.email);
// console.log("SALES")
// console.log(employeeOne.listSales(employeeOne.sales));
// console.log(employeeTwo.listSales(employeeTwo.sales));
// console.log(employeeThree.listSales(employeeThree.sales));
// console.log(employeeFour.listSales(employeeFour.sales));
// console.log("QUOTA")
// console.log(employeeOne.hasMetQuota());
// console.log(employeeTwo.hasMetQuota());
// console.log(employeeThree.hasMetQuota());
// console.log(employeeFour.hasMetQuota());
// console.log(employeeThree.willGetBonus());
// console.log(employeeFour.willGetBonus());

