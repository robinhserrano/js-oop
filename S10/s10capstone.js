class Person{
  constructor(){
  if(this.constructor === Person){
      throw new Error("Object cannot be created from an abstract class Person");
    }
  if(this.getFullName === undefined){
      throw new Error("To inherit from Person class, getFullName() must be implemented in the child class");
    }
  if(this.constructor === Employee && this.addRequest === undefined){
      throw new Error("addRequest() must be implemented in Employee class");
    }
  if(this.constructor === TeamLead && this.checkRequest === undefined){
      throw new Error("checkRequest() must be implemented in TeamLead class");
    }
  if(this.constructor === Admin && this.addUser === undefined){
      throw new Error("addUser() must be implemented in Admin class");
    }
  }
}

class Employee extends Person{
  #firstName
  #lastName
  #email
  #department
  #request  //Stretch goal (practice only)

  constructor(firstName, lastName,email,department){
  super()
  this.#firstName = firstName
  this.#lastName = lastName
  this.#email = email
  this.#department = department
  this.#request = []
  }
getFullName(){
  return `\n${this.#firstName} ${this.#lastName}`;
  }
setFirstName(firstName){
  this.#firstName = firstName;
  }
getFirstName(){
  return this.#firstName;
  }
setLastName(lastName){
  this.#lastName = lastName;
  }
getLastName(){
  return this.#lastName;
  }
setEmail(email){
  this.#email = email;
  }
getEmail(){
  return this.#email;
  }
setDepartment(department){
  this.#department = department;
  }
getDepartment(){
    return this.#department;
  }
setRequest(request){
    this.#request = request;
  }
getRequest(){  
    //console.log(this.#request)
    return this.#request
  }
//Required Methods: login(), logout(), addRequest()
login(){
  return `${this.getFullName()} has logged in`;
  }
logout(){
  return `${this.getFullName()} has logged out`;
  }
addRequest(request){
  this.#request.push(request);
  return 'Request has been added';
  }
}

class TeamLead extends Person{
  #firstName
  #lastName
  #email
  #department
  #members

  constructor(firstName, lastName,email,department){
  super()
  this.#firstName = firstName
  this.#lastName = lastName
  this.#email = email
  this.#department = department
  this.#members = []
  }
getFullName(){
  return `\n${this.#firstName} ${this.#lastName}`;
  }
setFirstName(firstName){
  this.#firstName = firstName;
  }
getFirstName(){
  return this.#firstName;
  }
setLastName(lastName){
  this.#lastName = lastName;
  }
getLastName(){
  return this.#lastName;
  }
setEmail(email){
  this.#email = email;
  }
getEmail(){
  return this.#email;
  }
setDepartment(department){
  this.#department = department;
  }
getDepartment(){
  return this.#department;
  }
setMembers(members){
  this.#members = members;
  }
getMembers(){
  return this.#members;
  }
//Required Methods: login(), logout(), addMember(), checkRequest()
login(){
  return `${this.getFullName()} has logged in`;
  }
logout(){
  return `${this.getFullName()} has logged out`
  }
addMember(member){
  this.#members.push(member);
  return this;
  }
checkRequest(index){
  return this.getMembers()[index].getRequest()
  }
}

class Admin extends Person{
  #firstName
  #lastName
  #email
  #department

  constructor(firstName, lastName,email,department){
  super()
  this.#firstName = firstName
  this.#lastName = lastName
  this.#email = email
  this.#department = department
  }
getFullName(){
  return `\n${this.#firstName} ${this.#lastName}`;
  }
setFirstName(firstName){
  this.#firstName = firstName;
  }
getFirstName(){
  return this.#firstName;
  }
setLastName(lastName){
  this.#lastName = lastName;
  }
getLastName(){
  return this.#lastName;
  }
setEmail(email){
  this.#email = email;
  }
getEmail(){
  return this.#email;
  }
setDepartment(department){
  this.#department = department;
  }
getDepartment(){
  return this.#department;
  }
//Required Methods: login(), logout(), addUser()
login(){
  return `${this.getFullName()} has logged in`;
  }
logout(){
  return `${this.getFullName()} has logged out`;
  }
addUser(){
  return 'User has been added';
  }
}

class Request{
  #name
  #requester
  #dateRequested
  #status

  constructor(name, requester,dateRequested,status){
  this.#name = name
  this.#requester = requester
  this.#dateRequested = dateRequested
  this.#status = status
  }
setName(name){
  this.#name = name;
  }
getName(){
  return this.#name;
  }
setRequester(requester){
  this.#requester = requester;
  }
getRequester(){
  return this.#requester;
  }
setDateRequested(dateRequested){
  this.#dateRequested = dateRequested;
  }
getDateRequested(){
  return this.#dateRequested;
  }
setStatus(status){
  this.#status = status;
  }
getStatus(){
  return this.#status;
  }
//Required Methods: updateRequest(), closeRequest(), cancelRequest()
updateRequest(){
  return `Request ${this.#name} has been updated`;
  }
closeRequest(){
  return `Request ${this.#name} has been closed`;
  }
cancelRequest(){
  return `Request ${this.#name} has been cancelled`;
  }
}

const employee1 = new Employee("John", "Doe", "djohn@mail.com", "Marketing")
const employee2 = new Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
const employee3 = new Employee("Robert", "Patterson", "probert@mail.com", "Sales")
const employee4 = new Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
const admin1 = new Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
const teamLead1 = new TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
const req1 = new Request("New hire orientation", teamLead1, "27-Jul-2021", "open")
const req2 = new Request("Laptop repair", employee1, "1-Jul-2021", "open")

console.log(employee1.getFullName()) 
console.log(admin1.getFullName()) 
console.log(teamLead1.getFullName()) 
console.log(employee2.login()) 
console.log("WIW", employee2.addRequest())
console.log(employee2.logout()) 
teamLead1.addMember(employee3) 
teamLead1.addMember(employee4) 
console.log(teamLead1) 
console.log(admin1.addUser())
req2.setStatus("closed") 
console.log(req2.closeRequest())

//stretch goals
employee3.addRequest(req1) 
employee3.addRequest(req2) 
console.log(teamLead1.checkRequest(0))