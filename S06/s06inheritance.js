class Person{
  constructor(firstName,lastName){
    this.firstName = firstName
    this.lastName = lastName
  }

  getFullname(){
    return `${this.firstName} ${this.lastName}`
  }
}

let person1 = new Person("John", "Smith")
console.log(person1.getFullname());

class Employee extends Person{
  constructor(employeeID, firstName, lastName){
    super(firstName, lastName);
    this.employeeID = employeeID
  }

  getEmployeeDetails(){
    console.log(`${this.employeeID} belongs to ${this.getFullname()}` )
  }
}

let employee1 = new Employee('Acme-001', 'John', 'Roberts')
console.log(employee1)

//Mini-exercise display full name of employee1
console.log(employee1.getFullname())
employee1.getEmployeeDetails();
console.log(employee1.employeeID)


class TeamLead extends Employee{
  constructor(employeeID, firstName, lastName){
    super(employeeID, firstName, lastName);
    this.members = []
  }

  addTeamMember(employee){
    this.members.push(employee)
    return this
  }

  getTeamMembers(){
    this.members.forEach(member=>{
      console.log(`${member.getFullname()}`)
    })
  return this
  }
}

//create a teamLead2
teamLead = new TeamLead('Acme-002', 'Jane Smith', 'jane@mail.com')
teamLead.getEmployeeDetails();

//Mini exercises

let employee2 = new Employee('Acme-003', 'Joe', 'Smith')
let employee3 = new Employee('Acme-004', 'Jessica', 'Smith')
let employee4 = new Employee('Acme-005', 'John', 'Smith')

teamLead.addTeamMember(employee2)
teamLead.addTeamMember(employee3)
teamLead.addTeamMember(employee4)

console.log(teamLead.getTeamMembers())

//Activity:
//1 Create a new class called Contractor
//contractor has the following name, email, contactNo
//A constructor also has a method call getContractorDetails and displayes the name and contact number
//of the contractor

//2 Create a new class called 

class Contractor{
  constructor(name, email, contractNo){
    this.name = name
    this.email = email
    this.contractNo = contractNo
  }

  getContractorDetails(){
      console.log(`Name: ${this.name}`)
      console.log(`Email: ${this.email}`)
      console.log(`Contract No: ${this.contractNo}`)
      return this
  }
}

class Subcontractor extends Contractor{
  constructor(name, email, contractNo,specializations){
    super(name, email, contractNo)
    this.specializations = specializations
  }

  getSubConDetails(){
    this.getContractorDetails()
    console.log(`${this.specializations}`)
  }
}

let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "091512456789",["gardens", "industrial"])
let subCont2 = new Subcontractor("Ace Bros", "acebros@mail.com", "091512456789",["hospitals", "bakeries", "libraries"])

subCont1.getSubConDetails()
subCont2.getSubConDetails()
