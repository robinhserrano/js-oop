//Mini Exercise:
//1 Create an abstract class called Person with an empty constructor and a wag to
//check if a method called is implemented 
//2. Create a class called employee from person with the following properties
/*
    property:  firstName, lastName, employeeID
    method: getfullName
*/


class Person{
  
  constructor(firstName, lastName,employeeID){
  this.firstName = firstName
  this.lastName = lastName
  this.employeeID = employeeID
  if(this.constructor === Person){
      throw new Error("Object cannot be created from an abstract class Person");
  }
  
  if(this.getFullName === undefined){
      throw new Error("Class must implement getFullName() method");
  }
}
}


class Employee extends Person{
  //To make properties directly inaccessible (as "private")
  //a # symbol must be added to the property must defined outside the constructor
  #firstName //adding the start of variable name makes the variable "private"
  #lastName
  #employeeID
  constructor(firstName, lastName,employeeID){
  super()
  this.#firstName = firstName
  this.#lastName = lastName
  this.#employeeID = employeeID
}
getFullName(){
  return `\n${this.#firstName} ${this.#lastName} has employeeID of ${this.#employeeID}`
}
setFirstName(firstName){
  this.#firstName = firstName
}

getFirstName(){
  return this.#firstName
}

setLastName(lastName){
  this.#lastName = lastName
}

getLastName(){
  return this.#lastName
}

setEmployeeID(employeeID){
  this.#employeeID = employeeID
}

getEmployeeID(){
  return this.#employeeID
}
}



//In order to access the "private" properties we need to create
//getters nd setters for each (Accessor methods)

const employee1 = new Employee("Jack", "Bauer", "Acme-00")
console.log(employee1.employeeID)
//Q2 can access the employeeId of employee1, can I change it? and if yes how?
employee1.employeeID = "Acme001"
console.log(employee1.employeeID)
//Q3 is this okay? NO


console.log(employee1.getFirstName())
employee1.setFirstName()
console.log(employee1.getFirstName())

//Mini-exercise
//hide the lastName and employee props
// create getters and setters for each

const employee2 = new Employee("Jill", "Hill", "Acme-02")
console.log(employee2.getFirstName())
console.log(employee2.getLastName())
employee2.setEmployeeID("Acme 200")
console.log(employee2.getFullName())


//Exercise 1:
//1 Recreate the RegularShape class
//2.Recreate the Square and Triangle classes from RegularShape
//3 Hid the props and create getters and setters for the props

class RegularShape{
  constructor(){
      if(this.constructor === RegularShape){
          throw new Error("Object cannot be instantiated from an abstract class RegularShape");
      }
          
      if(this.getPerimeter === undefined){
          throw new Error("Method getPerimeter needs to be implemented");
      }
  
      if(this.getArea === undefined){
          throw new Error("Method getArea needs to be implemented");
      }
    }
}


class Triangle extends RegularShape{
  #noSides 
  #length
  constructor(noSides,length){
      super()
      this.#noSides = noSides
      this.#length = length
     
      }
    getPerimeter(){
      return`the parameter of the triangle is ${3 * this.#length}` 
    }

    getArea(){
      return `the area of the triangle is ${(this.#length * this.#length  * Math.sqrt(3) /4).toFixed(3)}` 
    }

    getNoSides(){
      return this.#noSides
    }
    
    setNoSides(noSides){
      this.#noSides = noSides
    }

    getLength(){
      return this.#length
    }
    
    setLength(length){
      this.#length = length
    }
    
}

class Square extends RegularShape{
  #noSides 
  #length
  constructor(noSides,length){
      super()
      this.#noSides = noSides
      this.#length = length
     
      }
      getPerimeter(){
        return`the parameter of the square is ${4 * this.#length}` 
      }
      getArea(){
          return `the area of the square is ${this.#length * this.#length}`
      }

      getNoSides(){
        return this.#noSides
      }
      
      setNoSides(noSides){
        this.#noSides = noSides
      }
  
      getLength(){
        return this.#length
      }
      
      setLength(length){
        this.#length = length
      }
      
}

//Test Scenarios
let square1 = new Square(4,65);
console.log(square1.getLength())
console.log(square1.getPerimeter())

let triangle1 = new Triangle(3,15)
console.log(triangle1.getLength())
console.log(triangle1.getArea())

//Mini Exercise: find a way to reduce the numer of decimal places of the triangle to 3 decimal places

//Exercise 2 
//1 create a new class called User with an empty constructor and is an abstract
//class with methods login, register, logout
//2 create a new class from User called RegularUser with the following props and methods
/*
  properties: name, email, password
  methods: (required methods) - that output: <name> has logged in and <name>
  //has registered, <name> has logged out
      //browseJobs - outputs: there are 10 jobs found
*/
//3. Createa a new class from User called Admin with the following properties
/*
  properties: name, email, password, hasAdminExpired
  methods: (required methods)
      postJob - ouputs: Job added to site
*/


class User{
  constructor(){
      if(this.constructor === User){
          throw new Error("Object cannot be instantiated from an abstract class User");
      }
          
      if(this.login === undefined){
          throw new Error("Method login needs to be implemented");
      }
  
      if(this.register === undefined){
          throw new Error("Method register needs to be implemented");
      }

      if(this.logout === undefined){
        throw new Error("Method logout needs to be implemented");
    }

    }
}


class RegularUser extends User{
  #name 
  #email
  #password
  constructor(name,email,password){
      super()
      this.#name = name
      this.#email = email
      this.#password = password
      }

    browseJobs(){
      return 'outputs: there are 10 jobs found'
    }
    getName(){
      return this.#name
    }

    setName(name){
      this.#name = name
    }
    
    getEmail(){
      return this.#email
    }

    setEmail(email){
      this.#email = email
    }

    getPassword(password){
      this.#password = password
    }

    setPassword(){
      return this.#password
    }    
}

class AdminUser extends User{
  #name 
  #email
  #password
  #hasAdminExpired
  constructor(name,email,password, hasAdminExpired){
      super()
      this.#name = name
      this.#email = email
      this.#password = password
      this.#hasAdminExpired=hasAdminExpired
      }

    
    getName(){
      return this.#name
    }

    setName(name){
      this.#name = name
    }
    
    getEmail(){
      return this.#email
    }

    setEmail(email){
      this.#email = email
    }

    getPassword(){
      this.#password = password
    }

    setPassword(password){
      return this.#password
    }

    getHasAdminExpired(){
      return this.#hasAdminExpired
    } 
    setHasAdminExpired(hasAdminExpired){
      this.#hasAdminExpired=hasAdminExpired
    }

    postJob(){
      return 'Jobs added to site'
  }
}

const admin1 = new Admin("David C.", "davidc24@gmail.com", "test1234", false)
console.log(admin1.postJob())
admin1.setHasAdminExpired(true)
console.log(admin1.getHasAdminExpired())