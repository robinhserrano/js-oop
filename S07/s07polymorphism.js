class Person{
  constructor(firstName,lastName){
    this.firstName = firstName
    this.lastName = lastName
  }

  getFullname(){
    return `${this.firstName} ${this.lastName}`
  }
}

  
class Employee extends Person{
  constructor(firstName,lastName, employeeID){
  super(firstName, lastName)
  this.employeeID = employeeID
  }
  getFullname(){
    return super.getFullname() + `\nThe employee's name is ${this.firstName} ${this.lastName}`
  }
}

let employee1 = new Employee("Jack", "Smith", "Acme-001")
console.log(employee1.getFullname())


  
class TeamLead extends Employee{
  constructor(firstName,lastName, employeeID){
  super(firstName, lastName, employeeID)
  }
  getFullname(){
    return super.getFullname() + `\nI ${this.firstName} ${this.lastName} is the team lead`
  }
}

let teamLead1 = new TeamLead("John", "Doe", "Acme-002")
console.log(teamLead1.getFullname())


class Equipment{
  constructor(equipmentType){
  this.equipmentType = equipmentType //this.employeeID = employeeID
  }
  printInfo(){
    return `\nInfo: ${this.equipmentType}`
  }
}

class  Bulldozer extends Equipment{
  constructor(equipmentType,bladeType,model){
    super(equipmentType) 
    this.bladeType = bladeType
    this.model = model
    }

    printInfo(){
      return super.printInfo() + `\nThe bulldozer ${this.model} has a ${this.bladeType}`
    }
}

class TowerCrane extends Equipment{
  constructor(equipmentType,model, hookRadius, maxCapacity){
    super(equipmentType) 
   
    this.model = model
    this.hookRadius = hookRadius
    this.maxCapacity = maxCapacity
    }

    printInfo(){
      return super.printInfo() + `\nThe tower crane ${this.model} has a ${this.hookRadius} cm hook radius and  ${this.maxCapacity}  kg max capacity`
    }
}

class BackLoader extends Equipment{
  constructor(equipmentType,model, type, tippingLoad){
    super(equipmentType) 
   
    this.model = model
    this.type = type
    this.tippingLoad = tippingLoad
    }

    printInfo(){
      return super.printInfo() + `\nThe loader ${this.model} is a ${this.type} has a tipping load of ${this.tippingLoad} lbs `
    }
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel")
console.log(bulldozer1.printInfo())
let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towercrane1.printInfo())
let backLoader1 = new BackLoader("back loader", "Turtle", "hydraulic", 1500)
console.log(backLoader1.printInfo())



class RegularShape{
  constructor(noSides,length){
    this.noSides = noSides
    this.length = length
    
    }

    getPerimeter(){
      return `the parameter is `
    }

    getArea(){
      return `the area is `
    }
}


class Triangle extends RegularShape{
  constructor(noSides,length){
  super(noSides,length)
    }

    getPerimeter(){
      return super.getPerimeter() + (this.noSides * this.length) 
    }

    getArea(){

    return super.getArea() + (this.length * this.length  * Math.sqrt(3) /4)
    }
}

class Square extends RegularShape{
  constructor(noSides,length){
    super(noSides,length)
      }

      getPerimeter(){
        return super.getPerimeter() + (this.length * 4) 
      }
  
      getArea(){
      return super.getArea() +  + this.length * this.length;
      }


      
}

let triangle1 = new Triangle(3,10)
console.log(triangle1.getPerimeter())
console.log(triangle1.getArea())
let square1 = new Square(4,12);
console.log(square1.getPerimeter())
console.log(square1.getArea())